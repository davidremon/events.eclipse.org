items:
  - name: "Prof. Erol Gelenbe"
    anchor: erol_gelenbe
    title: "Institute of Theoretical and Applied Informatics Polish Academy of Sciences, Keynote"
    img: erol_gelenbe.jpg
    bio: |
      <p>Erol Gelenbe, Fellow of IEEE, ACM, IFIP, the Royal Statistical Society, and IET, graduated
      from TED Ankara Koleji, received the BS from METU (Ankara), the MS and PhD from the Polytechnic 
      Institute (New York University), and the DSc from the Sorbonne, Paris.</p>
      <p>He was awarded the ACM SIGMETRICS Life-Time Achievement Award that described him  "as the single individual who, over a 
      span of 30 years, has made the greatest overall contribution to the field of Computer System and 
      Network Performance Evaluation through original research, mentoring and doctoral training, creation 
      and direction of world class research groups, wide ranging international collaboration, and professional 
      service." Renowned for pioneering mathematical models of computer systems and networks, inventing G-Networks 
      and Random Neural Networks, he graduated over 90 PhDs, including 24 women. Professor at the Institute of 
      Theoretical and Applied Informatics, Polish Academy of Sciences, he has held  Chaired Professorships at 
      Imperial College, University of Central Florida, Duke University, Université Paris-Descartes, Paris-Saclay, 
      and Université de Liège (Belgium). </p>
      </p>He has impacted industry with contributions such as the Queueing Network 
      Analysis Package (Simulog), the XANTHOS fiber-optics local area network, the SYCOMORE distributed voice-packet 
      switch (Thales), the manufacturing simulator FLEXSIM, the C2 Agents (BAE Systems Ltd) software, and the Cognitive 
      Packet Network. Awarded “honoris causa” doctorates  from Università di Roma II (Italy), Bogazici University (Istanbul), 
      and Université de Liège (Belgium), the Parlar Foundation Science Award (2004), Grand Prix France Télécom (French Acad. 
      of Sciences), IET Oliver Lodge Medal UK, and “In Memoriam Dennis Gabor Award” (Novofer Foundation, Budapest), he is 
      Fellow of Academia Europaea, the French National Acad. of Technologies, the Royal Acad. of Belgium, the Science Academies 
      of Hungary, Poland and Turkey,  and Hon. Fellow of the Islamic Academy of Sciences. France awarded him Chevalier 
      de la Légion d’Honneur, Chevalier des Palmes Académiques, Commandeur du Mérite of France, and Italy honoured him with 
      the Commendatore al Merito and Grande Ufficiale dell’Ordine della Stella d'Italia. Currently serving as Associate Editor 
      of IEEE Transactions Cloud Computing, Acta Informatica, Computational Management Science, Performance Evaluation, 
      he previously served as Editor-in-Chief of the Computer Journal (BCS) and of SN Computer Science. His consultancies 
      have included INRIA, France-Telecom, Bull, IBM, Thomson CSF, Bell Labs, BT, General Dynamics UK, Huawei. Principal 
      Investigator in several EU research projects, Coordinator of FP7 NEMESYS and H2020 SerIoT, he also won numerous research 
      grants from NSF, ONR and ARO USA, EPSRC UK and industry. He is listed among 26 most influential scientists and 500 most 
      influential people in the Muslim world.</p>

  - name: "Alberto Marti"
    anchor: alberto_marti
    title: "VP of Open Source Community Relations @ OpenNebula, Keynote"
    img: alberto_marti.jpg
    bio: |
      <p>Alberto has developed most of his career in Spain and in the United Kingdom, both in the IT/FLOSS sector and 
      in Higher Education. As VP of Open Source Community Relations at OpenNebula, he deals with strategic collaborations 
      with public cloud/edge providers, other open source initiatives, and development teams from relevant vendors and 
      manufacturers. Alberto coordinates OpenNebula’s participation as a corporate member of the Cloud Native Computing 
      Foundation (CNCF), the Gaia-X Association, and the Structura-X project. He is one of the initial promoters of the 
      SovereignEdge.EU initiative, coordinating involvement in the Horizon Europe program and in other research and 
      innovation activities such as the IPCEI-CIS and the European Alliance for Industrial Data, Edge and Cloud, where 
      he supports the role of OpenNebula as chairing company of the Cloud/Edge Working Group. Alberto is particularly 
      interested in the conjunction between open source, ethical innovation, and digital sovereignty.</p>
      
  - name: "Sebastian Scholze"
    anchor: sebastian_scholze
    title: "Senior Researcher (ATB)"
    img: Sebastian_business.jpg
    bio: |
      <p>Sebastian Scholze studied Computer Science at the University of Bremen. Since 2000, he is working as 
      scientific staff member at ATB. He is Involved in diverse CEC funded RTD projects since the 5th FP. 
      Furthermore, he has long term project experience in several fields (e.g. Automotive, Manufacturing and 
      Logistics). He has excellent IT skills in object oriented programming languages, development methodologies 
      as well as in integration of complex software systems. Active in researching on context aware approaches 
      and systems, object-based software models and methodologies for optimising the software development process 
      for distributed, SOA and interoperable systems and web-based applications. He is working as project (e.g. 
      Self-Learning, AsKoWi, EngineeringWiki) and local project manager (e.g. K-Net, EPES) in several EU and direct 
      research projects. He was acting as coordinator of the FP7 Self-Learning project, H2020 U-Qasar project as 
      well as technical director of the H2020 project SAFIRE. He has more than 40 publications on technical and 
      research topics. He is currently acting as Coordinator of the H2020 projects ENCORE and SmartCLIDE.</p>

    
  - name: "Philippe Krief"
    anchor: philippe_krief
    title: "Research Relations Director (Eclipse Foundation)"
    img: 2020-11-PK-ID.jpg
    bio: |
      <p>Philippe Krief is the Research Relations Director of the Eclipse Foundation. He is a project 
      leader, expert in object-oriented design, embedded development, development tools, and Agile-Scrum 
      development process. He received his Ph.D. in Computer Science from University Paris 8, France in 1990.<br/>
      He has a passion for Eclipse since its beginning, early 2000 when he was involved in the early versions 
      of the Eclipse platform. Before joining the Eclipse Foundation staff, he worked 18 years for IBM as Senior 
      Architect, CALM evangelist, and R&D Development Manager.<br/>
      Today, Philippe is supervising 8 European research projects (H2020, ITEA3 & ECSEL), mainly for dissemination, 
      community building, and open-source best practices tasks.</p>
