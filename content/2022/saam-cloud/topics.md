---
title: "SAAM on Cloud - Technical topics of interest"
date: 2022-10-25T10:52:27-04:00
description: ""
categories: []
keywords: []
slug: ""
aliases: []
header_wrapper_class: "header-saam-2022-event"
toc: false
draft: false
---

{{< grid/section-container id="topics" class="featured-section-row">}}

{{< grid/div class="row featured-section-row-lighter-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### Cloud Computing Security and Privacy
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-lighter-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
![Cloud Computing Security and Privacy](../images/security-black.png) \
[Icon by F.Adiima from NounProject](https://thenounproject.com/search/?q=security&i=3349833)
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* Security practices, and architectures
* Secure discovery and authentication
* Cloud-centric threat models
* Cloud cryptography 
* Cloud access control and key management 
* Secure computation outsourcing 
* Integrity and verifiable computation
* Computation of encrypted data
* Secure cloud resource virtualization mechanisms 
* Trusted computing technology 
* Failure and Vulnerability  detection and prediction
* Energy/cost/efficiency of security in clouds 
* Availability, recovery and auditing
* Network security mechanisms (e.g., IDS etc.) for the cloud 
* Security and Privacy of Federated Clouds and Edge/Fog Computing

{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-light-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### Artificial Intelligence in Cloud Computing
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-light-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
![Artificial Intelligence in Cloud Computing](../images/ai-black.png) \
[Icon by priyanka from NounProject](https://thenounproject.com/search/?q=artificial%20intelligence&i=2858867)
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* Intelligent distributed architectures and infrastructures
* Context-Awareness and Location- Awareness 
* Machine Learning and Deep Learning Approaches  
* AI, deep learning for predictive security
* Challenges, Use Cases, and Solutions for Industry and Society
* Federated learning in the cloud-edge continuum
* AI-powered resource allocation and energy optimization in the cloud
* Deploying AI models in the clouds: Tools, techniques and approaches
* Distributed training of deep learning models
* Large Language Models in the cloud
* Real-time processing with AI on the cloud
* AI-powered data management in the cloud
* Leveraging cloud for advancing transparency and explainability of AI models
* AI accelerators on the cloud (e.g., TPUs)

{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-lighter-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### Cloud Computing Architecture
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-lighter-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
![Cloud Computing Architecture](../images/icon-architecture.png) \
[Icon by SBTS from NounProject](https://thenounproject.com/icon/web-architecture-1711291)
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* Cloud-oriented Architecture: Frameworks and Methodologies
* Cloud- and edge computing architectures for smart applications
* Microservices, monitoring and scalability of smart applications
* Service oriented architectures for smart applications
* Enterprise Cloud Computing Ecosystem: Technology, Architecture and Applications
* SaaS and Cloud Computing
* PaaS and IaaS Cloud Computing Service Models
* Serverless Cloud Computing Architecture and FaaS Model
* Building Cloud Computing Solutions at Scale
* The Cloud-to-Thing Continuum: Opportunities and Challenges 
* Green Cloud Computing Architecture
* Containerization and Cloud Deployment Model

{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-light-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### Modelling for Cloud Systems and Service
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-light-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
![Cloud Computing Models and Services](../images/modeling-black.png) \
[Icon by G.Tachfin from NounProject](https://thenounproject.com/search/?q=modelling&i=2710243)
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* Modelling Languages and Tools 
* Verification & Validation approaches
* Modelling for smart solutions
* Security & Privacy modelling
* Statistical models checking
* Modelling adaptive IoT systems 
* Runtime models
* Cloud programming and deployment models
* Power-aware profiling, modelling, and optimization for clouds
* Consistency, fault tolerance, and reliability models for clouds
* Cloud traffic characterization and measurements
* On-demand cloud computing models
* Challenges, Use Cases, and Solutions for Industry and Society
* Service-level agreements, business models, and pricing policies for cloud computing
* Open API and service orchestration on cloud platforms

{{</ grid/div >}}
{{</ grid/div >}}

{{</ grid/section-container >}}